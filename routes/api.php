<?php

use App\Http\Controllers\RegisterController;
use App\Http\Controllers\SessionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/test', function(Request $request) {
    return 'hello world from api';
});

Route::group([
    'prefix' => 'auth'
], function() {
        Route::post('register', [RegisterController::class, 'store']);
        Route::post('login', [SessionController::class, 'store']);
        Route::post('admin', [AdminController::class, 'store']);

});


