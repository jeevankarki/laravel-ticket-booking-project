<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * @return [type]
     */
    public function create()
    {
        return view('register.create');
    }
    
    /**
     * @return [type]
     */
    public function store()
    {
        $attributes = request()->validate([
            'username' => 'required|min:3|max:255|unique:users',
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'phone' => '',
            'password' => 'required|min:3|max:255',

        ]);

        $user = User::create($attributes);
        
        auth()->login($user);

        // return redirect('/')->with('success', 'Your account has been created.');
        return response()->json(['message' => 'Your account has been created successfully.'], 200);
    }
}
