<?php

namespace App\Http\Controllers;

use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class SessionController extends Controller
{
    /**
     * @return [type]
     */
    public function create()
    {
        return view('sessions.create');
    }
    
    /**
     * @return [type]
     */
    public function store()
    {
        $attributes = request()->validate([
            'username' => [
                'required',
                Rule::exists('users', 'username')->where(function ($query) {
                    $query->where('is_admin', 0);
                }),
            ],
            'password' => 'required'
        ]);

        if(! auth()->attempt($attributes)) {
            throw ValidationException::withMessages([
                'username', 'password' => 'Credentials did not match.'
            ]);

        }
        
        session()->regenerate();
        // return redirect('/')->with('success', 'You\'re Logged in.');
        return response()->json(['message' => 'Welcome back, You\'re Logged in.'], 200);

    }

    /**
     * @return [type]
     */
    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }

}
