<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

class AdminController extends Controller
{
    /**
     * @return [type]
     */
    public function create()
    {
        return view('admin.admin');
    }
    
    /**
     * @return [type]
     */
    public function store()
    {
        $attributes = request()->validate([
            'username' => [
                'required',
                Rule::exists('users', 'username')->where(function ($query) {
                    $query->where('is_admin', 1);
                }),
            ],
            'password' => 'required'
        ]);

        if(! auth()->attempt($attributes)) {
            throw ValidationException::withMessages([
                'username', 'password' => 'Credentials did not match for admin.'
            ]);
        }
        
        session()->regenerate();
        // return view('admin.dashboard')->with('success', 'You\'re Logged in.');
        return response()->json(['message' => 'Welcome Back Admin, You\'re Logged in.'], 200);

    }

    /**
     * @return [type]
     */
    public function destroy()
    {
        auth()->logout();

        return redirect('/');
    }
}
