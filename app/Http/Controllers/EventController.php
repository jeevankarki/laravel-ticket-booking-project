<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * @return [type]
     */
    public function index() 
    {
        return view('events', [
            'events' => Event::all()
        ]);
    }

    /**
     * @param Event $event
     * 
     * @return [type]
     */
    public function show(Event $event)
    {
        return view('event', [
            'event' => $event
        ]);
    }
}
