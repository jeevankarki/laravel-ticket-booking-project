<!DOCTYPE html>
    <title>Ticket Reservation System</title>
    <link rel="stylesheet" href="/css/app.css">
</head>
<body>
    <nav>
        <div style="float: left;">
            <a href="/">Logo</a>
        </div>
        <div style="padding-left: 400px;">
            <a href="/"><h1> Online Ticket Reservation System</h1></a>
        </div>
        @if (session()->has('success'))
    <div style="float: right; color: green;">
        <p>{{ session('success') }}</p>
    </div>
    @endif
        <div style="margin-left: 1100px;">
        @auth
        <form method="POST" action="/logout">
            @csrf
                <h3>Welcome, {{ auth()->user()->name }}!  <button  style="color: red;">   Logout</button></a> 
        @else
        <ul>
            <li>
                <a href="/register"><h3>Register</h3></a>
                <a href="/login"><h3>Login</h3></a>
            </li>
        </ul>    
        @endauth
        </div>
    </nav>
    <h2  style="margin-top: 20px;">Events</h2>

    @foreach ($events as $event)
        <section>
            <h1>
                <a href="/events/{{ $event->title }}">
                    {{ $event->title }}
                </a>
            </h1>

            <div>
                {{ $event->excerpt }}
            </div>
        </section>
    @endforeach
    
    
</body>

