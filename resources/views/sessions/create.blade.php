<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>

    <link rel="stylesheet" href="/css/register.css">
    <meta name="robots" content="noindex, follow">
</head>
<body>
    <div class="main">
        <section class="sign-in">
            <div class="container">
                <div class="signin-content">
                    <div class="signin-image">
                        <figure><img src="/img/signin-image.jpg" alt="sing up image"></figure>
                    </div>
                    <div class="signin-form">
                        <h2 class="form-title">Log In</h2>
                        <form method="POST" class="register-form" id="login-form" action="/login">
                            @csrf
                            <div class="form-group">
                                <label for="username"></label>
                                <input type="text" name="username" id="username" placeholder="Your Username" value="{{ old('username') }}" />
                                @error('username')
                                    <p style="color: red; font-size: small">{{ $message }}</p>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="password"></label>
                                <input type="password" name="password" id="password" placeholder="Password" />
                                @error('password')
                                    <p style="color: red; font-size: small">{{ $message }}</p>
                                @enderror
                            </div>

                            <div class="form-group form-button">
                                <input type="submit" name="login" id="login" class="form-submit" value="Login" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</body>
</html>