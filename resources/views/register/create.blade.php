<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Registration</title>


<link rel="stylesheet" href="/css/register.css">
<meta name="robots" content="noindex, follow">
</head>
<body>
<div class="main">

<section class="signup">
<div class="container">
<div class="signup-content">
<div class="signup-form">
<h2 class="form-title">Sign up</h2>
<form method="POST" class="register-form" id="register-form" action="/register">
    @csrf
<div class="form-group">
<label for="username"></label>
<input type="text" name="username" id="username" placeholder="Your Username" value="{{ old('username') }}" />
@error('username')
    <p style="color: red; font-size: small">{{ $message }}</p>
@enderror
</div>
<div class="form-group">
<label for="name"></label>
<input type="text" name="name" id="name" placeholder="Your Name" value="{{ old('name') }}" />
@error('name')
    <p style="color: red; font-size: small">{{ $message }}</p>
@enderror
</div>
<div class="form-group">
<label for="email"></label>
<input type="email" name="email" id="email" placeholder="Your Email" value="{{ old('email') }}" />
@error('email')
    <p style="color: red; font-size: small">{{ $message }}</p>
@enderror
</div>
<div class="form-group">
<label for="phone"></label>
<input type="text" name="phone" id="phone" placeholder="Your Phone" value="{{ old('phone') }}" />
</div>
<div class="form-group">
<label for="password"></label>
<input type="password" name="password" id="password" placeholder="Password" />
@error('password')
    <p style="color: red; font-size: small">{{ $message }}</p>
@enderror
</div>

<div class="form-group form-button">
<input type="submit" name="register" id="register" class="form-submit" value="Register" />
</div>
</form>
</div>
<div class="signup-image">
<figure><img src="/img/signup-image.jpg" alt="sing up image"></figure>
<a href="/login" class="signup-image-link">I am already member</a>
</div>
</div>
</div>
</section>
