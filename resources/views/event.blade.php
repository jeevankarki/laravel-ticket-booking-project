<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ticket Reservation System</title>
    <link rel="stylesheet" href="/app.css">
</head>
<body>    
    <section>
        <h1>{{ $event->title }}</h1>
        <div>
            {!! $event->body !!}
        </div>
    </section>
    <a href="/">Go Back</a>
</body>
</html>
